#!/bin/bash

PROPERTY_FILE="/home/ansible/hebb/environments/BCNE/dev-vra/payor/bcbsne_payor_env.properties"

function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`
   cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
   echo $PROP_VALUE
}

connectorProtocol=$(getProperty connectorProtocol) 
connectorHost=$(getProperty connectorHost) 
connectorPort=$(getProperty connectorPort) 
connectorHBRestServiceUser=$(getProperty connectorHBRestServiceUser) 
connectorHBRestServicePass=$(getProperty connectorHBRestServicePass) 
payorUser=$(getProperty payorUser)
payorHost=$(getProperty payorHost)
weblogicJarsDir=$(getProperty weblogicDomainDir)/jars
weblogicMappingFile=$(getProperty weblogicDomainDir)/data/defaultpathnamemapping.txt


 
ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#CONNECTOR_PROTOCOL#|${connectorProtocol}|g' ${weblogicJarsDir}/HBIDPolicyConfig.properties" 

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#CONNECTOR_HOST#|${connectorHost}|g' ${weblogicJarsDir}/HBIDPolicyConfig.properties" 

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#CONNECTOR_PORT#|${connectorPort}|g' ${weblogicJarsDir}/HBIDPolicyConfig.properties" 

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#CONNECTOR_HB_REST_USER#|${connectorHBRestServiceUser}|g' ${weblogicJarsDir}/HBIDPolicyConfig.properties" 

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#CONNECTOR_HB_REST_PASSWORD#|${connectorHBRestServicePass}|g' ${weblogicJarsDir}/HBIDPolicyConfig.properties"

echo "Replacing defaultpathnamemapping on $payorHost"
ssh -q  ${payorUser}@${payorHost} "echo -n >  ${weblogicMappingFile}"
ssh -q  ${payorUser}@${payorHost} "cat > ${weblogicMappingFile} << EOF
COBPolicy=COB {0}
COBPolicy.policyType=COBPolicyType
COBPolicy.benefitPlanType=COBPlanType
COBPolicy.dateRanges.responsibilitySequenceCode=COBPriority
COBPolicy.dateRanges.startDate=COBStartDate
COBPolicy.dateRanges.endDate=COBEndDate
EOF"

exit 0
