HE_DIR=$1
echo "BCBSNE SH : hecommon_migration_custom_jobs.sh start"

mv $HE_DIR/etc/com.healthedge.customer.generic.extract.bcbsne.account.cfg $HE_DIR/etc/com.healthedge.he.common.extract.bcbsne.account.cfg
mv $HE_DIR/etc/com.healthedge.customer.generic.extract.bcbsne.alternateaddress.cfg $HE_DIR/etc/com.healthedge.he.common.extract.bcbsne.alternateaddress.cfg
mv $HE_DIR/etc/com.healthedge.customer.generic.extract.bcbsne.bobcob.cfg $HE_DIR/etc/com.healthedge.he.common.extract.bcbsne.bobcob.cfg
mv $HE_DIR/etc/com.healthedge.customer.generic.extract.bcbsne.claimpaymentextract.cfg $HE_DIR/etc/com.healthedge.he.common.extract.bcbsne.claimpaymentextract.cfg
mv $HE_DIR/etc/com.healthedge.customer.generic.extract.bcbsne.eligibility.cfg $HE_DIR/etc/com.healthedge.he.common.extract.bcbsne.eligibility.cfg
mv $HE_DIR/etc/com.healthedge.customer.generic.extract.bcbsne.eligibilityinc.cfg $HE_DIR/etc/com.healthedge.he.common.extract.bcbsne.eligibilityinc.cfg
mv $HE_DIR/etc/com.healthedge.customer.generic.extract.bcbsne.oossupplier.cfg $HE_DIR/etc/com.healthedge.he.common.extract.bcbsne.oossupplier.cfg
mv $HE_DIR/etc/com.healthedge.customer.generic.extract.bcbsne.voidpayment.cfg $HE_DIR/etc/com.healthedge.he.common.extract.bcbsne.voidpayment.cfg

echo "BCBSNE SH : hecommon_migration_custom_jobs.sh end"